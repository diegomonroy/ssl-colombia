<!-- Begin Newsletter -->
	<section class="newsletter" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'newsletter' ); ?>
			</div>
		</div>
	</section>
<!-- End Newsletter -->