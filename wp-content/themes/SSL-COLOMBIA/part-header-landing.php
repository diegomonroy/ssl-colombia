<!-- Begin Top -->
	<section class="top_1" data-wow-delay="0.5s">
		<div class="row align-center align-middle">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'top_1' ); ?>
			</div>
		</div>
	</section>
	<section class="top_2" data-wow-delay="0.5s">
		<div class="row align-center align-middle">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'top_landing' ); ?>
			</div>
		</div>
	</section>
<!-- End Top -->