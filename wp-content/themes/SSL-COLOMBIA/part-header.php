<!-- Begin Top -->
	<section class="top_1" data-wow-delay="0.5s">
		<div class="row align-center align-middle">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'top_1' ); ?>
			</div>
		</div>
	</section>
	<section class="top_2" data-wow-delay="0.5s">
		<div class="row align-middle">
			<div class="small-12 medium-3 columns">
				<?php if ( is_front_page() ) : ?><div class="logo_hide"><?php endif; ?>
					<?php dynamic_sidebar( 'logo' ); ?>
				<?php if ( is_front_page() ) : ?></div><?php endif; ?>
			</div>
			<div class="small-12 medium-8 columns">
				<?php get_template_part( 'part', 'menu' ); ?>
			</div>
		</div>
	</section>
<!-- End Top -->