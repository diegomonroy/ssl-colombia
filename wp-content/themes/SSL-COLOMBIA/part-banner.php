<!-- Begin Banner -->
	<section class="banner" data-wow-delay="0.5s">
		<div class="row collapse expanded">
			<div class="small-12 columns">
				<?php if ( is_front_page() ) : dynamic_sidebar( 'banner_inicio' ); endif; ?>
				<?php if ( is_page( array( 'quienes-somos' ) ) ) : dynamic_sidebar( 'banner_quienes_somos' ); endif; ?>
				<?php if ( is_page( array( 'que-hacemos' ) ) ) : dynamic_sidebar( 'banner_que_hacemos' ); endif; ?>
				<?php if ( is_page( array( 'como-lo-hacemos' ) ) ) : dynamic_sidebar( 'banner_como_lo_hacemos' ); endif; ?>
				<?php if ( is_page( array( 'mision-y-vision' ) ) ) : dynamic_sidebar( 'banner_mision' ); endif; ?>
				<?php if ( is_page( array( 'vision' ) ) ) : dynamic_sidebar( 'banner_vision' ); endif; ?>
				<?php if ( is_page( array( 'nuestros-valores' ) ) ) : dynamic_sidebar( 'banner_nuestros_valores' ); endif; ?>
				<?php if ( is_page( array( 'suministro-de-personal-en-aseo-y-cafeteria' ) ) ) : dynamic_sidebar( 'banner_suministro_de_personal_en_aseo_y_cafeteria' ); endif; ?>
				<?php if ( is_page( array( 'trabajos-especiales' ) ) ) : dynamic_sidebar( 'banner_trabajos_especiales' ); endif; ?>
				<?php if ( is_page( array( 'mantenimiento-tecnico-locativo' ) ) ) : dynamic_sidebar( 'banner_mantenimiento_tecnico_locativo' ); endif; ?>
				<?php if ( is_page( array( 'suministros-corporativos' ) ) ) : dynamic_sidebar( 'banner_suministros_corporativos' ); endif; ?>
				<?php if ( is_page( array( 'mantenimientos-locativos-y-de-obra-civil' ) ) ) : dynamic_sidebar( 'banner_mantenimientos_locativos_y_de_obra_civil' ); endif; ?>
				<?php if ( is_page( array( 'limpieza-y-mantenimiento-de-fachadas' ) ) ) : dynamic_sidebar( 'banner_limpieza_y_mantenimiento_de_fachadas' ); endif; ?>
				<?php if ( is_page( array( 'suministro-instalacion-y-mantenimiento-de-sistemas-contra-incendios' ) ) ) : dynamic_sidebar( 'banner_suministro_instalacion_y_mantenimiento_de_sistemas_contra_incendios' ); endif; ?>
				<?php if ( is_page( array( 'suministro-instalacion-y-mantenimiento-de-aires-acondicionados' ) ) ) : dynamic_sidebar( 'banner_suministro_instalacion_y_mantenimiento_de_aires_acondicionados' ); endif; ?>
				<?php if ( is_page( array( 'manejo-integrado-de-plagas-fumigacion-y-desinfeccion' ) ) ) : dynamic_sidebar( 'banner_manejo_integrado_de_plagas_fumigacion_y_desinfeccion' ); endif; ?>
				<?php if ( is_page( array( 'senalizacion-demarcacion-y-recubrimiento-acrilico' ) ) ) : dynamic_sidebar( 'banner_senalizacion_demarcacion_y_recubrimiento_acrilico' ); endif; ?>
				<?php if ( is_page( array( 'suministro-e-instalacion-de-mobiliario-de-oficina' ) ) ) : dynamic_sidebar( 'banner_suministro_e_instalacion_de_mobiliario_de_oficina' ); endif; ?>
				<?php if ( is_page( array( 'trabajos-especiales-2' ) ) ) : dynamic_sidebar( 'banner_trabajos_especiales_2' ); endif; ?>
			</div>
		</div>
	</section>
<!-- End Banner -->