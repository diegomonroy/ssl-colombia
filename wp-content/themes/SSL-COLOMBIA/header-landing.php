<!DOCTYPE html>
<html class="no-js" lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title><?php bloginfo(title); ?></title>
		<!-- Begin Open Graph Protocol -->
		<meta property="og:url" content="<?php echo site_url(); ?>">
		<meta property="og:type" content="website">
		<meta property="og:title" content="<?php bloginfo(name); ?>">
		<meta property="og:description" content="<?php bloginfo(description); ?>">
		<meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/build/logo_ogp.png">
		<link rel="image_src" href="<?php echo get_template_directory_uri(); ?>/build/logo_link_ogp.png">
		<script type="application/ld+json">
			{
				"@context": "http://schema.org",
				"@type": "Organization",
				"name": "SSL COLOMBIA",
				"url": "https://www.sslcolombia.com",
				"address": "",
				"sameAs": [
					"https://www.facebook.com/SSLCOLOMBIA/",
					"https://www.instagram.com/sslcolombia.sas/",
					"https://www.linkedin.com/company/ssl-colombia-sas/"
				]
			}
		</script>
		<!-- End Open Graph Protocol -->
		<?php wp_head(); ?>
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/build/favicon.ico">
		<link rel="alternate" hreflang="es-es" href="<?php echo get_site_url(); ?>/">
		<!-- Begin Google Analytics -->
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-155987079-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', 'UA-155987079-1');
		</script>
		<!-- End Google Analytics -->
	</head>
	<body>
		<?php get_template_part( 'part', 'header-landing' ); ?>