<?php

/*

Template Name: Landing

*/

?>
<?php get_header( 'landing' ); ?>
	<?php get_template_part( 'part', 'content-landing' ); ?>
<?php get_footer( 'landing' ); ?>