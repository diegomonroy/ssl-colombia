<?php
$column = '';
if ( ! is_product() ) {
	$column = 'medium-9';
}
?>
<!-- Begin Content -->
	<section class="content products" data-wow-delay="0.5s">
		<div class="row">
			<?php if ( ! is_product() ) : ?>
			<div class="small-12 medium-3 columns">
				<?php dynamic_sidebar( 'left' ); ?>
			</div>
			<?php endif; ?>
			<div class="small-12 <?php echo $column; ?> columns">
				<div class="woocommerce">
					<?php woocommerce_content(); ?>
				</div>
			</div>
		</div>
	</section>
<!-- End Content -->