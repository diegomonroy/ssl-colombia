<?php

/*

Functions for my template

*/

/*
 * Function to add my styles files
 */
function my_styles_files() {
	wp_enqueue_style( 'foundation-css', get_template_directory_uri() . '/build/foundation/css/foundation.min.css', false );
	wp_enqueue_style( 'animate-css', get_template_directory_uri() . '/build/bower_components/animate.css/animate.min.css', false );
	wp_enqueue_style( 'fancybox-css', get_template_directory_uri() . '/build/bower_components/fancybox/dist/jquery.fancybox.min.css', false );
	wp_enqueue_style( 'font-awesome-css', get_template_directory_uri() . '/build/bower_components/font-awesome/css/font-awesome.min.css', false );
	if ( is_child_theme() ) {
		wp_enqueue_style( 'parent-css', trailingslashit( get_template_directory_uri() ) . 'style.css', false );
	}
	wp_enqueue_style( 'theme-css', get_stylesheet_uri(), false );
}
add_action( 'wp_enqueue_scripts', 'my_styles_files' );

/*
 * Function to add my scripts files
 */
function my_scripts_files() {
	//wp_deregister_script( 'jquery' );
}
add_action( 'wp_enqueue_scripts', 'my_scripts_files' );

/*
 * Function to add file types to uploads
 */
function add_file_types_to_uploads( $file_types ) {
	$new_filetypes = array();
	$new_filetypes[ 'svg' ] = 'image/svg+xml';
	$new_filetypes[ 'webp' ] = 'image/webp';
	$file_types = array_merge( $file_types, $new_filetypes );
	return $file_types;
}
add_filter( 'upload_mimes', 'add_file_types_to_uploads' );

/*
 * Function to add my scripts files in footer
 */
function my_scripts_files_footer() {
	//wp_enqueue_script( 'jquery-js', get_template_directory_uri() . '/build/bower_components/jquery/dist/jquery.min.js', false );
	wp_enqueue_script( 'what-input-js', get_template_directory_uri() . '/build/bower_components/what-input/dist/what-input.min.js', false );
	wp_enqueue_script( 'foundation-js', get_template_directory_uri() . '/build/foundation/js/vendor/foundation.min.js', false );
	wp_enqueue_script( 'fancybox-js', get_template_directory_uri() . '/build/bower_components/fancybox/dist/jquery.fancybox.min.js', false );
	wp_enqueue_script( 'wow-js', get_template_directory_uri() . '/build/bower_components/wow/dist/wow.min.js', false );
	wp_enqueue_script( 'theme-js', get_template_directory_uri() . '/build/app.js', false );
}
add_action( 'wp_footer', 'my_scripts_files_footer' );

/*
 * Function to register my menus
 */
function register_my_menus() {
	register_nav_menus(
		array(
			'main-menu' => __( 'Main Menu' )
		)
	);
}
add_action( 'init', 'register_my_menus' );

/*
 * Function to register my sidebars and widgetized areas
 */
function arphabet_widgets_init() {
	register_sidebar(
		array(
			'name' => 'Top 1',
			'id' => 'top_1',
			'before_widget' => '<div class="moduletable_to1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Logo',
			'id' => 'logo',
			'before_widget' => '<div class="moduletable_to2 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Menu',
			'id' => 'menu',
			'before_widget' => '<div class="moduletable_to3">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Top Landing',
			'id' => 'top_landing',
			'before_widget' => '<div class="moduletable_to4">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Inicio',
			'id' => 'banner_inicio',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Quiénes Somos',
			'id' => 'banner_quienes_somos',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Qué Hacemos',
			'id' => 'banner_que_hacemos',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner ¿Cómo lo Hacemos?',
			'id' => 'banner_como_lo_hacemos',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Misión',
			'id' => 'banner_mision',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Visión',
			'id' => 'banner_vision',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Nuestros Valores',
			'id' => 'banner_nuestros_valores',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Suministro de Personal en Aseo y Cafetería',
			'id' => 'banner_suministro_de_personal_en_aseo_y_cafeteria',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Trabajos Especiales',
			'id' => 'banner_trabajos_especiales',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Mantenimiento Técnico Locativo',
			'id' => 'banner_mantenimiento_tecnico_locativo',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Suministros Corporativos',
			'id' => 'banner_suministros_corporativos',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Mantenimientos Locativos y de Obra Civil',
			'id' => 'banner_mantenimientos_locativos_y_de_obra_civil',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Limpieza y Mantenimiento de Fachadas',
			'id' => 'banner_limpieza_y_mantenimiento_de_fachadas',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Suministro, Instalación y Mantenimiento de Sistemas Contra Incendios',
			'id' => 'banner_suministro_instalacion_y_mantenimiento_de_sistemas_contra_incendios',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Suministro, Instalación y Mantenimiento de Aires Acondicionados',
			'id' => 'banner_suministro_instalacion_y_mantenimiento_de_aires_acondicionados',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Manejo Integrado de Plagas. Fumigación y Desinfección',
			'id' => 'banner_manejo_integrado_de_plagas_fumigacion_y_desinfeccion',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Señalización, Demarcación y Recubrimiento Acrílico',
			'id' => 'banner_senalizacion_demarcacion_y_recubrimiento_acrilico',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Suministro e Instalación de Mobiliario de Oficina',
			'id' => 'banner_suministro_e_instalacion_de_mobiliario_de_oficina',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Trabajos Especiales',
			'id' => 'banner_trabajos_especiales_2',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Left',
			'id' => 'left',
			'before_widget' => '<div class="moduletable_le1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 1',
			'id' => 'block_1',
			'before_widget' => '<div class="moduletable_b11">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 2',
			'id' => 'block_2',
			'before_widget' => '<div class="moduletable_b21">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 3',
			'id' => 'block_3',
			'before_widget' => '<div class="moduletable_b31">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 4',
			'id' => 'block_4',
			'before_widget' => '<div class="moduletable_b41">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Newsletter',
			'id' => 'newsletter',
			'before_widget' => '<div class="row align-center align-middle moduletable_n1">',
			'after_widget' => '</div>',
			'before_title' => '<h2>',
			'after_title' => '</h2>'
		)
	);
	register_sidebar(
		array(
			'name' => 'Bottom',
			'id' => 'bottom',
			'before_widget' => '<div class="moduletable_bo1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
}
add_action( 'widgets_init', 'arphabet_widgets_init' );

/*
 * Function to declare WooCommerce support
 */
function woocommerce_support() {
	add_theme_support( 'woocommerce' );
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}
add_action( 'after_setup_theme', 'woocommerce_support' );

/*
 * Functions to declare WooCommerce image sizes
 */
function woocommerce_catalog_image( $size ) {
	return array(
		'width' => 220,
		'height' => 220,
		'crop' => 0,
	);
}
add_filter( 'woocommerce_get_image_size_thumbnail', 'woocommerce_catalog_image' );

function woocommerce_single_image( $size ) {
	return array(
		'width' => 600,
		'height' => 600,
		'crop' => 0,
	);
}
add_filter( 'woocommerce_get_image_size_single', 'woocommerce_single_image' );

function woocommerce_gallery_image( $size ) {
	return array(
		'width' => 200,
		'height' => 200,
		'crop' => 0,
	);
}
add_filter( 'woocommerce_get_image_size_gallery_thumbnail', 'woocommerce_gallery_image' );

/*
 * Function to declare WooCommerce products per page
 */
function new_loop_shop_per_page( $cols ) {
	$cols = 9;
	return $cols;
}
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

/*
 * Function to declare WooCommerce products per column
 */
if ( ! function_exists( 'loop_columns' ) ) {
	function loop_columns() {
		$number = 3;
		return $number;
	}
}
add_filter( 'loop_shop_columns', 'loop_columns' );

/*
 * Custom hooks in WooCommerce
 */
function custom_description() {
	echo '
		<p class="text-justify"><small><strong>Por favor tenga en cuenta que estos precios son actualizados diariamente de acuerdo a la TRM del día según el Banco de la Republica.</strong></small></p>
		<p class="text-justify"><a href="https://www.sslcolombia.com/integrador/" target="_blank" class="button">SOY INTEGRADOR</a></p>
	';
	the_content();
}
add_action( 'woocommerce_single_product_summary', 'custom_description', 10 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

function custom_clear() {
	$producto_caracteristicas = get_field( 'producto_caracteristicas' );
	if ( $producto_caracteristicas != '' ) {
		$tab_1 = '<li class="tabs-title is-active"><a href="#panel1" aria-selected="true">Características</a></li>';
		$tab_content_1 = '<div class="tabs-panel is-active" id="panel1">' . $producto_caracteristicas . '</div>';
	} else {
		$tab_1 = '';
		$tab_content_1 = '';
	}
	$producto_informacion_adicional = get_field( 'producto_informacion_adicional' );
	if ( $producto_informacion_adicional != '' ) {
		$tab_2 = '<li class="tabs-title"><a data-tabs-target="panel2" href="#panel2">Información Adicional</a></li>';
		$tab_content_2 = '<div class="tabs-panel" id="panel2">' . $producto_informacion_adicional . '</div>';
	} else {
		$tab_2 = '';
		$tab_content_2 = '';
	}
	$producto_ficha_tecnica = get_field( 'producto_ficha_tecnica' );
	if ( $producto_ficha_tecnica != '' ) {
		$tab_3 = '<li class="tabs-title"><a data-tabs-target="panel3" href="#panel3">Ficha Técnica</a></li>';
		$tab_content_3 = '<div class="tabs-panel" id="panel3"><p class="text-center"><a href="' . $producto_ficha_tecnica . '" target="_blank"><i class="fa-solid fa-file-pdf"></i><br />VER PDF</a></p></div>';
	} else {
		$tab_3 = '';
		$tab_content_3 = '';
	}
	$producto_manual_de_instalacion = get_field( 'producto_manual_de_instalacion' );
	if ( $producto_manual_de_instalacion != '' ) {
		$tab_4 = '<li class="tabs-title"><a data-tabs-target="panel4" href="#panel4">Manual de Instalación</a></li>';
		$tab_content_4 = '<div class="tabs-panel" id="panel4"><p class="text-center"><a href="' . $producto_manual_de_instalacion . '" target="_blank"><i class="fa-solid fa-file-pdf"></i><br />VER PDF</a></p></div>';
	} else {
		$tab_4 = '';
		$tab_content_4 = '';
	}
	$html = '
		<div class="clear"></div>
		<ul class="tabs" data-tabs id="product-tabs">
			' . $tab_1 . '
			' . $tab_2 . '
			' . $tab_3 . '
			' . $tab_4 . '
			<li class="tabs-title"><a data-tabs-target="panel5" href="#panel5">Términos de Envío y Entrega</a></li>
		</ul>
		<div class="tabs-content" data-tabs-content="product-tabs">
			' . $tab_content_1 . '
			' . $tab_content_2 . '
			' . $tab_content_3 . '
			' . $tab_content_4 . '
			<div class="tabs-panel" id="panel5">
				<p class="text-justify">Para brindar diferentes soluciones a nuestros clientes ponemos a su disposición las diferentes formas de entrega y envió. Las cuales podrá elegir con uno de nuestros asesores, según su necesidad.</p>
				<p class="text-justify">Entrega presencial directamente desde nuestras oficinas en horario de lunes a viernes de 8:00 am a 5:00 pm., o solicitando directamente con nuestros asesores el domicilio al lugar en que lo requiera, en tal caso el cliente deberá suministrar toda la información necesaria para coordinar la entrega del producto. (Es importante mencionar que el despacho se realiza 24 horas después de realizar el pago).</p>
				<p class="text-justify">Para entregas a nivel nacional ponemos a su disposición una transportadora especializada, certificada y autorizada. Tenga en cuenta de que si el envió se realiza por una de las transportadoras autorizadas se enviara su respectivo soporte mediante correo electrónico o vía WhatsApp. Es responsabilidad del cliente verificar el estado del envió directamente con la transportadora seleccionada, de igual forma ponemos a su disposición nuestra línea de soporte para brindarles todo el apoyo que requieran.</p>
				<ul>
					<li>El costo del transporte será asumido por el cliente y se incluirá dentro del valor del producto al momento de que se gestione el pago.</li>
					<li>En SSL Colombia S.A.S. nos hacemos responsables hasta el momento en que el producto es entregado a la transportadora seleccionada. O a nuestro cliente final, en el caso de que se maneje toda la entrega directamente con nosotros. (Es importante aclarar que el costo adicional bien sea por envió, garantía y/o venta serán asumidos por el cliente).</li>
					<li>En casos de perdida, daños o deterioros del producto que puedan ser ocasionados en el transporte será únicamente bajo el riesgo y responsabilidad del cliente, además de proceder con la respectiva reclamación a la transportadora de ser el caso.</li>
				</ul>
			</div>
		</div>
		<div class="text-center">
			<button class="hollow button" onclick="window.history.go(-1);">Volver</button>
		</div>
	';
	echo $html;
}
add_action( 'woocommerce_after_single_product_summary', 'custom_clear', 10 );